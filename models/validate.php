<?php 
class validate{

	public function validateAlturaParede($alturaParede){
		if(($alturaParede - 1.90) < 0.30){
			$erro = "A altura da parede tem que ser no minimo 30 centimetros maior que a altura da porta";
			header("Location: ../Views/calculo-tintas.php?error=$erro");
			exit();
		} 
	}

	public function validateAreaParede($areaParede){
		if($areaParede < 1){
			$erro = "A aréa da parede não pode ser menor do que 1m²";
			header("Location: ../Views/calculo-tintas.php?error=$erro");
			exit();
		} 
		else if($areaParede > 50){
			$erro = "A aréa da parede não pode ser maior do que 50m²";
			header("Location: ../Views/calculo-tintas.php?error=$erro");
			exit();
		} 
	}

	public function validacaoItensParedes($areaPortaJanela, $areaParede){
		if($areaPortaJanela > ($areaParede / 2)){
			$erro = "A area das janelas é 50% maior que a área da parede";
			header("Location: ../Views/calculo-tintas.php?error=$erro");
			exit();
		}
	}
}